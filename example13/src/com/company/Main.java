package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Company company = new Company(actor1, "troupe");

        actor1.print();
        company.printActor();

        actor1 = null;
        company = null;

    }

}
