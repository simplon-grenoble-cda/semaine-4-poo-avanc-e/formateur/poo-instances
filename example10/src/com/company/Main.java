package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");

        actor1.print();
        actor2.print();

        myMethod(actor1);
        actor1.print();
    }

    public static void myMethod(Actor param) {
        param.print();
        param = new Actor("Charlie");
        param.print();
    }
}
