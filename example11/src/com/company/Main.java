package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");

        actor1.print();
        actor2.print();

        actor1 = myMethod();
        actor1.print();
    }

    public static Actor myMethod() {
        Actor anActor = new Actor("Charlie");
        anActor.print();
        return anActor;
    }
}
