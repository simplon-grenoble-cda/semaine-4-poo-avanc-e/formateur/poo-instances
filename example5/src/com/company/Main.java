package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");
        Actor actor3 = actor1;
        actor1 = null;


        actor2.print();
        actor1.print();
        // Comment est ce que je peux afficher à nouveau l'instance dont le nom est "Alice" ?
    }
}
