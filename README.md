Le but de ces exercices est de comprendre pas à pas, au fur et à mesure de l'exécution du programme, ce qu'il se passe en terme
d'instances d'objets en mémoire, et de référence à ces objets dans le contexte d'exécution (les symboles, càd noms de variables, ou
d'attributs, qui permettent d'accéder aux instances).

Pour chaque petit programme : 

- Faites une représentation schématique, pas par pas de l'exécution, de l'état de la mémoire, des 
  symboles dans le contexte d'exécution et des liens entre ceux ci et la mémoire. 
- Grace à ce schéma, prédisez (sans exécuter le programme), ce qui va s'afficher dans la console
- Vérifier en lançant le programme que le résultat corresponde


