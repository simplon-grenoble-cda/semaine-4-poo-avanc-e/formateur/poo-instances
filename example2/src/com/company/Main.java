package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");

        Actor actor3 = actor1;
        actor3.setName("Charlie");

        actor1.print();
        actor2.print();
        actor3.print();
    }
}
