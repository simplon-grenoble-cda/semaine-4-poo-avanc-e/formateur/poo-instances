package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Alice");
        Company company = new Company(actor2, "troupe");

        actor1.print();
        actor1.setName("Bob");
        company.printActor();
        actor1.print();
    }
}
