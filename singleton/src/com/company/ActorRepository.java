package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is design to behave as a singleton. It stores
 * a list of available actors (unique list of Actor instances to share
 * across the application)
 */
public class ActorRepository {
    protected List<Actor> availableActors;
    protected static ActorRepository singleton;

    private ActorRepository() {
        this.availableActors = new ArrayList<>();
    }

    public static ActorRepository getSingleton() {
        if (singleton == null) {
            /* create the singleton instance if not existing already */
            singleton = new ActorRepository();
        }
        return singleton;
    }

    public void addActor(Actor actor) {
        this.availableActors.add(actor);
    }

    public List<Actor> getAvailableActors() {
        return this.availableActors;
    }

}
