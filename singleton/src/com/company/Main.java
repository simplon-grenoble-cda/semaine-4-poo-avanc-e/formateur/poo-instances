package com.company;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        ActorRepository actorRepository = ActorRepository.getSingleton();
        List<Actor> availableActors = actorRepository.getAvailableActors();

        availableActors.add(new Actor("Alice"));
        availableActors.add(new Actor("Bob"));

        printAvailableActors();
    }

    public static void printAvailableActors() {
        ActorRepository actorRepository = ActorRepository.getSingleton();
        for (Actor actor : actorRepository.getAvailableActors()) {
            actor.print();
        }
    }
}
