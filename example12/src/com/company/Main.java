package com.company;

//import mylib.myMethod;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");

        actor1 = myMethod();
        actor2 = myMethod();


        actor1.setName("David");

        actor1.print();
        actor2.print();
    }


    public static Actor myMethod() {
        Actor anActor = new Actor("Charlie");
        return anActor;
    }
}
