package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Company company1 = new Company(actor1, "Paris");
        Company company2 = new Company(new Actor("Bob"), "Lyon");




        Company.referenceActor.print();
        company1.renameReferenceActor("Charlie");
        company1.renameMainActor("David");

        company1.printActor();
        company1.printReferenceActor();

        company2.printActor();
        company2.printReferenceActor();
    }

}
