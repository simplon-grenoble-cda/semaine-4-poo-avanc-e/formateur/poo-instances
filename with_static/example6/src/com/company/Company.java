package com.company;

public class Company {
    protected Actor mainActor;
    protected String companyName;
    static Actor referenceActor = new Actor("Reference");

    public Company(Actor mainActor, String companyName) {
        this.mainActor = mainActor;
        this.companyName = companyName;
    }

    public Company() {
    }

    public void setMainActor(Actor mainActor) {
        this.mainActor = mainActor;
    }

    public Actor getMainActor() {
        return mainActor;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void printActor() {
        this.mainActor.print();
    }

    public void renameMainActor(String newName) {
        this.mainActor.setName(newName);
    }

    public void printReferenceActor() {
        referenceActor.print();
    }

    public static void renameReferenceActor(String newName) {
        referenceActor.setName(newName);
    }
}
