package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");

        Company.renameReferenceActor("Charlie");

        Company company1 = new Company(actor1, "Paris");
        Company company2 = new Company(new Actor("Bob"), "Lyon");


        company1.printActor();
        company1.printReferenceActor();

        company2.printActor();
        company2.printReferenceActor();
    }

}
