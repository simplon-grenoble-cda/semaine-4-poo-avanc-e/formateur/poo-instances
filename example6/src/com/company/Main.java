package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");
        Actor actor3 = new Actor("Charlie");


        Actor actor4 = actor1;
        actor4.setName("David");
        actor4 = actor2;
        actor4.setName("Eve");
        actor4 = actor3;
        actor4.setName("Greg");
        actor4 = null;
        actor3 = new Actor("Frank");


        actor1.print();
        actor2.print();
        actor3.print();
        actor4.print();
    }
}
