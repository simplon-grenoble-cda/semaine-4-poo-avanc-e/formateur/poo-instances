package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");


        actor1.print();
        actor2.print();

        actor1 = actor2;

        actor1.print();
        actor2.print();

        // Comment est ce que je peux afficher à nouveau l'instance dont le nom est "Alice" ?
    }
}
