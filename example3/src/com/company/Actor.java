package com.company;

public class Actor {
    protected String name;

    public Actor(String name) {
        this.name = name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void print() {
        System.out.format("Object Actor #%X\n", System.identityHashCode(this));
        System.out.format("Name : %s\n", this.name);
        System.out.println("----\n");
    }
}
