package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Company {
    protected List<Actor> actors;
    protected String companyName;

    public Company(String companyName) {
        this();
        this.companyName = companyName;
    }

    public Company() {
        this.actors = new ArrayList<>();
    }

    public List<Actor> getActors() {
        return this.actors;
    }

    public void addActor(Actor actor) {
        this.actors.add(actor);
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void renameActor(String newActorName, int actorIdx) {
        this.actors.get(actorIdx).setName(newActorName);
    }

    public void printActors() {
        for (Actor actor : this.actors) {
            actor.print();
        }
    }


}
