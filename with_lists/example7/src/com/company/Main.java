package com.company;

public class Main {

    public static void main(String[] args) {
        Company company1 = new Company("Paris");
        Company company2 = new Company("Lyon");


        company1.addActor(new Actor("Alice"));
        company2.addActor(new Actor("Alice"));

        company1.renameActor("Bob", 0);
        company2.printActors();
    }

}
