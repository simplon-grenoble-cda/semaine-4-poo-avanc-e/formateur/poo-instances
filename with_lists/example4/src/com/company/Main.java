package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Actor> actorList = new ArrayList<>();

        actorList.add(new Actor("Alice"));
        actorList.add(new Actor("Bob"));

        actorList.get(0).print();
    }
}
