package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Actor> actorList = new ArrayList<>();

        Actor actor1 = new Actor("Alice");
        actorList.add(actor1);

        actor1 = new Actor("Bob");
        actorList.add(actor1);

        Actor actor2 = actorList.get(1);
        actor2.print();
    }
}
