package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Actor> actorList1 = new ArrayList<>();

        actorList1.add(new Actor("Alice"));
        actorList1.add(new Actor("Bob"));

        List<Actor>actorList2 = new ArrayList<>(actorList1);

        actorList1.get(0).setName("Charlie");

        for (Actor actor : actorList2) {
            actor.print();
        }
    }
}
