package com.company;

public class Main {

    public static void main(String[] args) {
        Actor actor1 = new Actor("Alice");

        Company company1 = new Company("Paris");
        Company company2 = new Company("Lyon");


        company1.addActor(actor1);
        company2.addActor(actor1);

        company1.renameActor("Bob", 0);
        company2.printActors();
    }

}
