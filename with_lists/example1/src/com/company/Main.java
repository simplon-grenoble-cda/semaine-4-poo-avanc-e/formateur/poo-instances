package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Actor> actorList = new ArrayList<>();

        Actor actor1 = new Actor("Alice");
        Actor actor2 = new Actor("Bob");

        actorList.add(actor1);
        actorList.add(actor2);

        Actor actor3 = actorList.get(0);
        actor3.print();
    }
}
