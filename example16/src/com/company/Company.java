package com.company;

public class Company {
    protected Actor mainActor;
    protected String companyName;

    public Company(Actor mainActor, String companyName) {
        this.mainActor = mainActor;
        this.companyName = companyName;
    }

    public Company() {
    }

    public void setMainActor(Actor mainActor) {
        this.mainActor = mainActor;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void printActor() {
        this.mainActor.print();
    }

    public void renameMainActor(String newName) {
        this.mainActor.setName(newName);
    }
}
